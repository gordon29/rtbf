/**
 @file gpuBF/Refocus.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-05-05

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef REFOCUS_CUH_
#define REFOCUS_CUH_

#include <cublas_v2.h>

#include "Operator.cuh"

namespace rtbf {

// // Create an enum to select synthesis mode
// enum class DecoderMode { AdjointRamp = 0, Tikhonov = 1 };

/** @brief Class to apply REFoCUS to data. The output is always
demodulated focused channel data.

This class applies focusing delays and optional apodization profiles to the
input data, applying coherent summation to synthesize either the transmit or
receive aperture. The input is of size (nsamps, nxmits, nchans). The output is
of size (nrows, ncols, N), where N depends on the SynthesisMode:
  - SynthTx:  N = nchans
  - SynthRx:  N = nxmits

The input data must be complex, but can be either modulated or baseband.
(NOTE: Baseband has the advantage that the output only has to be Nyquist sampled
with respect to the bandwidth, not the modulation frequency, without loss of
information.)
  - To focus modulated data, specify samplesPerCycle = 0.f.
  - To focus baseband data, specify samplesPerCycle to be with respect to the
    demodulation frequency.

Apodization profiles are optional.
*/
template <typename T_in, typename T_out>
class Refocus : public Operator<Tensor<T_in>, Tensor<T_out>> {
 protected:
  // CUDA objects
  int gpuID;
  cublasHandle_t handle;  ///< CUBLAS handle
  cufftHandle fftplan1;   ///< CUFFT plan object
  cufftHandle fftplan2;   ///< CUFFT plan object
  // DecoderMode dmode;       ///< Decoder mode (e.g., AdjointRamp)
  size_t nsamps;  ///< Number of samples in the original image
  size_t nxmits;  ///< Number of transmit events
  size_t nelemT;  ///< Number of transmit elements
  size_t nelemR;  ///< Number of receive elements
  Tensor<cuda::std::complex<float>> inv;  ///< Transmitter delays decoder
  Tensor<cuda::std::complex<float>> tmp;  ///< Dummy array

  // /// @brief Construct the decoder need to retrieve multistatic data.
  // void makeDecoder();

 public:
  /// @brief Direct constructor with decoder
  Refocus(std::shared_ptr<Tensor<T_in>> input,
          const Tensor<cuda::std::complex<float>> *h_invTx,
          cudaStream_t cudaStream = 0, std ::string moniker = "Refocus",
          std::string loggerName = "");
  virtual ~Refocus();

  /// @brief Decode the encoded transmit to recover the multistatic data.
  void decode();
};

namespace kernels::Refocus {
/// @cond KERNELS
/**	@addtogroup Refocus Kernels
        @{
*/
/** @brief Naive kernel for out-of-place matrix transpose. Can optimize if
 necessary.
 @param src Pointer to source matrix
 @param dst Pointer to destination matrix
 @param nx_dst Inner dimension size for destination matrix
 @param ny_dst Outer dimension size for destination matrix
 @param alpha Multiplicative factor (scalar)
*/
template <typename T_in, typename T_out>
__global__ void transpose(T_in *A, T_out *B, int nx, int ny);

/** @}*/
/// @endcond

}  // namespace kernels::Refocus

// Add template type traits for SFINAE
template <typename>
struct is_Refocus : std::false_type {};
template <typename T_in, typename T_out>
struct is_Refocus<Refocus<T_in, T_out>> : std::true_type {};

}  // namespace rtbf

#endif /* REFOCUS_CUH_ */
