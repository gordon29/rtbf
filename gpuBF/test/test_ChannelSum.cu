/**
 @file test/test_ChannelSum.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-08-17
*/

#include "ChannelSum.cuh"
#include "gtest/gtest.h"

namespace rtbf {
namespace ChannelSumTest {
namespace cstd = cuda::std;

using MyTypes = ::testing::Types<short, int, float, cstd::complex<short>,
                                 cstd::complex<int>, cstd::complex<float>>;

// Use a test fixture to reuse resource between tests
template <typename T>
class ChannelSumTest : public ::testing::Test {
 protected:
  // Re-use the same arrays for all tests
  const std::vector<size_t> dims = {11, 5, 3, 7, 1, 2};
  const size_t numel = prod(dims);
  std::vector<T> data;   // Input data
  std::vector<T> h_out;  // CPU output
  std::vector<T> d_out;  // GPU output
  std::shared_ptr<Tensor<T>> h_ptr;
  std::shared_ptr<Tensor<T>> d_ptr;
  // Set up the test suite
  ChannelSumTest() {
    data.resize(numel);   // Input data
    h_out.resize(numel);  // CPU output
    d_out.resize(numel);  // GPU output
    // spdlog::set_level(spdlog::level::debug);  // For more debugging output
    initializeArray(data);
    // Copy this data to a Tensor
    h_ptr = std::make_shared<Tensor<T>>(dims, -1, "CPU data");
    d_ptr = std::make_shared<Tensor<T>>(dims, 0, "GPU data");
    h_ptr->copyFrom(data.data());
    d_ptr->copyFrom(data.data());
  }
  // Tear down the test suite
  ~ChannelSumTest() {}
};

template <typename T>
void initializeArray(std::vector<T> &datvec) {
  for (auto &d : datvec) d = T(1);
}

template <typename T>
double compute_sqerr(const std::vector<T> &a, const std::vector<T> &b) {
  double sqerr = 0.0;
  size_t n = a.size();
  for (size_t i = 0; i < n; i++) {
    double err = abs(a[i] - b[i]);
    sqerr += err * err;
  }
  return sqerr;
}

template <typename T>
T getReal(T x) {
  return x;
}
template <typename T>
T getReal(cstd::complex<T> x) {
  return real(x);
}
template <typename T>
T getReal(std::complex<T> x) {
  return real(x);
}

TYPED_TEST_SUITE(ChannelSumTest, MyTypes);

TYPED_TEST(ChannelSumTest, SumEveryAxis) {
  int n = static_cast<int>(dims.size());
  for (int axis = -n; axis < n; axis++) {
    auto ax = axis < 0 ? axis + n : axis;
    int nchout = 1;
    int ds = dims[ax] / nchout;
    ChannelSum<TypeParam, TypeParam> SH{h_ptr, axis, nchout, 0, "ChSumCPU"};
    ChannelSum<TypeParam, TypeParam> SD{d_ptr, axis, nchout, 0, "ChSumGPU"};
    SH.sumChannels(false);
    SD.sumChannels(false);
    SH.getOutputRaw()->copyTo(h_out.data());
    SD.getOutputRaw()->copyTo(d_out.data());
    // Check result
    int onumel = numel / dims[ax] * nchout;
    EXPECT_NEAR(compute_sqerr(h_out, d_out), 0, 0.001);
    EXPECT_EQ(getReal(h_out[0]), ds);
  }
}

TYPED_TEST(ChannelSumTest, UnevenOutputChannels) {
  for (int axis = 0; axis < dims.size(); axis++) {
    int nchout = 5;
    ChannelSum<TypeParam, TypeParam> SH{h_ptr, axis, nchout, 0, "ChSumCPU"};
    ChannelSum<TypeParam, TypeParam> SD{d_ptr, axis, nchout, 0, "ChSumGPU"};
    SH.sumChannels(false);
    SD.sumChannels(false);
    SH.getOutput()->copyTo(h_out.data());
    SD.getOutput()->copyTo(d_out.data());
    // Check result
    if (nchout > dims[axis]) nchout = dims[axis];
    int ds = dims[axis] / nchout;
    int onumel = numel / dims[axis] * nchout;
    EXPECT_NEAR(compute_sqerr(h_out, d_out), 0, 0.001);
    EXPECT_EQ(getReal(h_out[0]), ds);
  }
}

}  // namespace ChannelSumTest
}  // namespace rtbf
