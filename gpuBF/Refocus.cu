/**
 @file gpuBF/Refocus.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-05-05

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Refocus.cuh"

namespace rtbf {

template <typename T_in, typename T_out>
Refocus<T_in, T_out>::Refocus(
    std::shared_ptr<Tensor<T_in>> input,
    const Tensor<cuda::std::complex<float>> *h_invTx, cudaStream_t cudaStream,
    std ::string moniker,
    std::string loggerName) {  // If previously initialized, reset
  this->label = moniker;
  this->setLogger(loggerName);
  this->logdebug("Initializing {}.", this->label);

  static_assert(std::is_same_v<T_in, cuda::std ::complex<float>> &&
                    std::is_same_v<T_in, T_out>,
                "Refocus currently requires complex float input and output.");

  // Read inputs
  this->in = input;                        // Share ownership of input Tensor
  this->stream = cudaStream;               // Set asynchronous stream
  gpuID = this->in->getDeviceID();         // Execute on input's device
  auto idims = this->in->getDimensions();  // Get dimensions of input

  // Infer input data properties. It is expected that the data will be organized
  // as [number of RF samples, number of transmits, number of elements].
  if (idims.size() != 3)
    this->logerror(
        "Input to Refocus must be 3D: [nsamps, nxmits, nelemR]. Received {}.",
        vecString(idims));
  nsamps = idims[0];
  nxmits = idims[1];
  nelemR = idims[2];
  inv = Tensor<cuda::std::complex<float>>(*h_invTx, gpuID,
                                          this->label + "->inv", this->logName);
  auto invdims = inv.getDimensions();
  if (invdims.size() != 3 || invdims[1] != nxmits || invdims[2] != nsamps)
    this->logerror(
        "Transmit decoder must be 3D: [nelemT, nxmits, nsamps]. Received {}.",
        vecString(invdims));
  nelemT = invdims[0];

  // Define data dimensions
  std::vector<size_t> tdims = {nsamps, nelemR, std::max(nelemT, nxmits)};
  std::vector<size_t> odims = {nsamps, nelemT, nelemR};

  // Initialize output array and other private arrays
  tmp = Tensor<cuda::std::complex<float>>(tdims, gpuID, this->label + "->tmp",
                                          this->logName);
  this->out = std::make_shared<Tensor<T_out>>(
      odims, gpuID, this->label + "->out", this->logName);

  // Initialize CUDA objects
  CCE(cublasCreate(&handle));                  // Initialize cublas handle
  CCE(cublasSetStream(handle, this->stream));  // Initialize cublas stream
  CCE(cufftCreate(&fftplan1));
  CCE(cufftCreate(&fftplan2));
  int p = nsamps;            // Number of rows of data pitch
  int y1 = nxmits * nelemT;  // Number of columns of valid data
  int y2 = nelemT * nelemR;  // Number of channels in data
  CCE(cufftPlanMany(&fftplan1, 1, &p, &p, 1, p, &p, 1, p, CUFFT_C2C, y1));
  CCE(cufftPlanMany(&fftplan2, 1, &p, &p, 1, p, &p, 1, p, CUFFT_C2C, y2));
  CCE(cufftSetStream(fftplan1, this->stream));
  CCE(cufftSetStream(fftplan2, this->stream));
}

template <typename T_in, typename T_out>
Refocus<T_in, T_out>::~Refocus() {
  this->loginfo("Destroyed {}.", this->label);
}

template <typename T_in, typename T_out>
void Refocus<T_in, T_out>::decode() {
  if (gpuID < 0)
    this->template logerror<NotImplemented>(
        "No CPU implementation of Refocus yet.");
  // Set CUDA device
  CCE(cudaSetDevice(gpuID));
  cufftComplex one{1.f}, zero{0.f};
  int nfreqs = nsamps;

  // Apply forward FFT to the raw data in the sample dimension
  static_assert(std::is_same_v<T_in, cuda::std ::complex<float>> &&
                    std::is_same_v<T_in, T_out>,
                "Refocus currently requires complex float input and output.");
  auto *rptr = reinterpret_cast<cufftComplex *>(this->in->data());
  auto *tptr = reinterpret_cast<cufftComplex *>(tmp.data());
  auto *optr = reinterpret_cast<cufftComplex *>(this->out->data());
  CCE(cufftExecC2C(fftplan1, rptr, tptr, CUFFT_FORWARD));

  // Transpose the raw data to prepare for matrix multiplication
  // (nsamps, nxmits, nelemR) --> (nxmits, nelemR, nsamps)
  dim3 B1(16, 16);
  dim3 G1((nxmits * nelemR - 1) / B1.x + 1, (nsamps - 1) / B1.y + 1);
  kernels::Refocus::transpose<<<G1, B1, 0, this->stream>>>(
      tptr, rptr, nxmits * nelemR, nfreqs);
  tmp.resetToZerosAsync(this->stream);

  // Matrix multiply using batched and strided Cgemm3m
  // A: inv (nelemT, nxmits, nfreqs)
  // B: raw (nxmits, nelemR, nfreqs)
  // C: out (nelemT, nelemR, nfreqs)
  // Compute C = alpha*A*B + beta*C on a batched/strided basis
  auto *A = reinterpret_cast<cufftComplex *>(inv.data());
  int lda = nelemT;
  int strideA = inv.getPitch() * nxmits;
  auto *B = rptr;
  int ldb = nxmits;
  int strideB = ldb * nelemR;
  auto *C = tptr;
  int ldc = nelemT;
  int strideC = ldc * nelemR;
  CCE(cublasCgemm3mStridedBatched(handle, CUBLAS_OP_N, CUBLAS_OP_N, nelemT,
                                  nelemR, nxmits, &one, A, lda, strideA, B, ldb,
                                  strideB, &zero, C, ldc, strideC, nfreqs));

  // Transpose the multistatic dataset back to (nfreqs, nelemT, nelemR)
  dim3 B2(16, 16);
  dim3 G2((nfreqs - 1) / B2.x + 1, (nelemT * nelemR - 1) / B2.y + 1);
  kernels::Refocus::transpose<<<G2, B2, 0, this->stream>>>(tptr, optr, nfreqs,
                                                           nelemT * nelemR);

  // Undo the FFT to get back our multistatic data array
  CCE(cufftExecC2C(fftplan2, optr, optr, CUFFT_INVERSE));
}

///////////////////////////////////////////////////////////////////////////
// kernels
///////////////////////////////////////////////////////////////////////////

template <typename T_in, typename T_out>
__global__ void kernels::Refocus::transpose(T_in *src, T_out *dst, int nx_dst,
                                            int ny_dst) {
  // Determine information about the current thread
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;
  if (x < nx_dst && y < ny_dst) dst[x + nx_dst * y] = src[y + ny_dst * x];
}

// TODO: Add support for other input, output data types.
template class Refocus<cuda::std::complex<float>, cuda::std::complex<float>>;
}  // namespace rtbf